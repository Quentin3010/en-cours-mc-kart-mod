# Cubic Racers

Quentin BERNARD

## Description du projet

Le projet "Cubic Racers" est un mod Minecraft développé en Java, dont l'objectif est d'apporter une expérience de jeu similaire à celle de Mario Kart dans l'univers de Minecraft. Ce mod introduit des karts entièrement fonctionnels, offrant aux joueurs la possibilité de rouler, tourner, faire marche arrière, drifter, planer grâce à un deltaplane, tout en bénéficiant d'animations fluides et de l'utilisation d'objets spéciaux.

En plus des mouvements de base, les karts seront capables de réaliser des actions spéciales, comme le drift, qui leur permettra d'effectuer des virages serrés avec style et précision. De plus, les joueurs pourront utiliser un deltaplane pour planer dans les airs, ajoutant une dimension supplémentaire aux courses.

Un aspect important du projet sera également la mise en place d'animations fluides pour les karts, afin de leur donner vie et de rendre l'expérience de conduite encore plus immersive.

Enfin, les karts pourront utiliser des objets spéciaux, inspirés du concept des "objets" dans Mario Kart. Ces objets offriront des avantages stratégiques aux joueurs, tels que des boosts de vitesse, des attaques contre les autres joueurs, des obstacles pour ralentir leurs adversaires, et bien plus encore.

Vidéo de présentation : lien

## Fonctionnalités

- Les karts sont fonctionnels.
	- On peut les invoquer à l'aide d'un objet.
	- On peut monter à bord des karts.
	- On peut avancer, reculer et tourner avec les karts.
	- On peut déraper et contrôler l'intensité du dérapage.
	- On peut activer un mode delta plane lorsque le kart est en l'air et voler avec.
- Les karts sont animés.
	- Les roues tournent dans un sens qui dépend de l'avancement ou de la marche arrière du kart.
	- Les roues pivotent lorsque nous tournons à gauche ou à droite.
	- Le mode delta plane peut être déployé et rétracté.
	- Une petite hélice émerge du kart et tourne lorsqu'il est dans l'eau.
- Les caisses d'objets et les objets sont implémentés.
	- Les caisses d'objets peuvent être placées à l'aide d'un objet.
	- Objet banane : immobilise le kart en cas de collision.
	- Objet champignon : augmente la vitesse du kart.
	- Objet étoile : augmente la vitesse du kart, rend le kart insensible aux attaques et étourdit les karts lors des collisions.
	- Objet bombe-omb : une bombe qui explose tous les véhicules dans un certain rayon. L'explosion se produit en cas de collision avec la bombe ou après un certain laps de temps.
	- Objet klaxon : immobilise tous les véhicules dans un rayon autour du kart qui l'utilise.
	- Objet éclair : immobilise tous les karts sauf celui qui l'utilise.
	- Objet carapace verte : Une carapace qui fonce tout droit et peut rebondir trois fois sur les murs avant de se détruire. Si elle heurte un kart, elle l'immobilise avant de se détruire.
	- Objet Faux cube : Un faux cube à objet qui immobilise celui qui le heurte au lieu de lui donner un objet.

À l'avenir, mon objectif est d'intégrer un système de création de circuits dans le mod. Cela permettrait d'introduire une notion de classement (1er, 2ème, 3ème, etc.) et d'ajouter des objets spéciaux qui utilisent ces informations, tels que les carapaces rouges.


## Mon rôle

Au sein du projet "Cubic Racers", mon rôle principal est de prendre en charge la conception technique du mod. Alors que Maxmos se concentre sur la création des modèles et des textures des karts, et que Turbo se charge de leur animation, ma responsabilité consiste à assurer que le mod fonctionne de manière fluide et cohérente sur le plan technique.

Je suis chargé de concevoir l'architecture du mod, de mettre en place les mécanismes de jeu nécessaires et d'assurer l'intégration de toutes les fonctionnalités. Cela implique de travailler sur la logique de contrôle des karts, la gestion des mouvements, la détection des collisions, la manipulation des objets spéciaux, ainsi que tout autre aspect technique crucial pour l'expérience de jeu globale.


